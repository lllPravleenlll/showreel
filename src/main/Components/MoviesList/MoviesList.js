import React, { Component } from "react";
import "./MoviesList.css";
import MovieCard from "../MovieCard/MovieCard";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVideo } from "@fortawesome/free-solid-svg-icons";

export default class MoviesList extends Component {
  render() {
    const sampleArray = new Array(10).fill(1);
    return (
      <div className="container-fluid mt-md-5 mt-2 px-2 px-md-3">
        <div className="w-100 mb-3">
          <span className="h4 text-muted">
            <FontAwesomeIcon icon={faVideo}  color={"#008cef"}/> &nbsp;
            All movies
          </span>
        </div>

        <div className="row px-0">
          {sampleArray.map((movie) => (
            <div className="col-md-3 col-6 mb-4 ">
              <MovieCard />
            </div>
          ))}
        </div>
        <div className="jumbotron"></div>
        <div className="jumbotron"></div>
        <div className="jumbotron"></div>
        <div className="jumbotron"></div>
      </div>
    );
  }
}
