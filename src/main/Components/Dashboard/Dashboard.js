import React, { Component } from "react";
import "./Dashboard.css";
import TrendingMovie from "../TrendingMovie/TrendingMovie";
import MoviesList from "../MoviesList/MoviesList";
import MostWatchedMovies from "../MostWatchedMovies/MostWatchedMovies";

export default class Dashboard extends Component {
  render() {
    return (
      <div className="container-fluid px-2 px-md-3">
        {/* <TrendingMovie />
        <MoviesList /> */}

        <div className="row">
          <div className="col-md-3 d-none d-md-flex">
            <div className="jumbotron side-movies-container movie-container pt-3">
              <MostWatchedMovies />
            </div>
          </div>

          <div className="col-md-9 col-12 ">
            <div className="container-fluid px-0">
              <TrendingMovie />
              <MoviesList />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
