import React from "react";
import "./Navbar.css";

export default function Navbar() {
  return (
    <div className="custom-navbar py-md-4 py-2 px-md-5 px-0 mb-3">
      <div className="row w-100 px-0">
        <div className="col-6 mr-auto">
          <div className="navbar-brand ">Showreel</div>
        </div>
        <div className="col-6 text-right ml-auto">
          <div className="movie-option">Add a movie</div>
        </div>
      </div>
    </div>
  );
}
