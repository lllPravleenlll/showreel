import React, { Component } from "react";
import "./MovieCard.css";
import SampleImage from "../../assets/latest_movie_banner.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

export default class MovieCard extends Component {
  getMostWatchedMovieCard = () => {
    return (
      <div className="custom-card-mostwatched w-100">
        <div className="row no-gutters">
          <div className="col-5">
            <img
              src={SampleImage}
              className="img-thumbnail custom-img-thumbnail-mostwatched p-2"
            />
          </div>

          <div class="col-7">
            <div className="px-3 py-2  mt-3 movie-intro-container-mostwatched">
              <div className="w-75 text-left">Movie title</div>
              <div className="w-50 text-right" style={{ fontSize: "0.7rem" }}>
                4.5{" "}
                <FontAwesomeIcon icon={faStar} size={"md"} color={"#ffdf00"} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  getCommonMovieCard = () => {
    return (
      <div className="custom-card w-100">
        <img
          src={SampleImage}
          className="img-thumbnail custom-img-thumbnail p-0"
        />
        <div className="px-3 py-2 movie-intro-container">
          <div className="w-100 text-left">Movie title</div>
          <div className="w-100 text-right" style={{ fontSize: "0.7rem" }}>
            4.5 <FontAwesomeIcon icon={faStar} size={"md"} color={"#ffdf00"} />
          </div>
        </div>

        <p className="px-3 all-movie-description w-100">
          This holds the description of the movie. This holds the description of
          the movie
        </p>
      </div>
    );
  };

  render() {
    return (
      <>
        {this.props.mostWatched
          ? this.getMostWatchedMovieCard()
          : this.getCommonMovieCard()}
      </>
    );
  }
}
