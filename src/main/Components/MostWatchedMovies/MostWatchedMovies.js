import React, { Component } from "react";
import "./MostWatchedMovies.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFire } from "@fortawesome/free-solid-svg-icons";
import MovieCard from "../MovieCard/MovieCard";

export default class MostWatchedMovies extends Component {
  render() {
    const sampleArray = new Array(5).fill(1);

    return (
      <div className="container-fluid w-100 px-0">
        <div className="mt-2 sticky-top bg-white">
          <FontAwesomeIcon icon={faFire} size="lg" color={"#fa5353"} />
          &nbsp;&nbsp;
          <span className="heading text-muted">Most watched</span>
        </div>
        <ul className="list-group border-0 list-group-flush mt-4 mostwatched-movie-list">
          {sampleArray.map((_) => (
            <li className="list-group-item px-0 border-0 mb-2">
              <MovieCard mostWatched />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
