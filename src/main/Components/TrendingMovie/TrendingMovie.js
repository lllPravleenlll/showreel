import React, { Component } from "react";
import "./TrendingMovie.css";
import LatestMovie from "../../assets/latest_movie_banner.jpg";

export default class TrendingMovie extends Component {
  render() {
    return (
      <div className="jumbotron bg-danger p-0 trendimg-movie-container">
        <img
          src={'https://wallpaper.dog/large/10915262.jpg'}
          alt="latest_movie"
          className="latest-movie-img"
        />
        <div className="img-overlay p-4">
          <div className="container-custom px-0 bg-transparent ">
            <h2 className="movie-title">Movie title</h2>
            <p className="movie-description">
              This hold the description of the movie . This hold the description
              of the movie . This hold the description of the movie 
            </p>
            <button className="btn btn-primary">Watch now</button>
          </div>
        </div> 
      </div>
    );
  }
}
