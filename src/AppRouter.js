import { Route } from "react-router-dom";
import Dashboard from "./main/Components/Dashboard/Dashboard";

const ROUTES = [
  {
    path: "/",
    component: Dashboard,
  },
];

const createRoutes = () => {
  let createdRoutes = ROUTES.map((eachRoute, index) => (
    <Route
      key={"route_" + index.toString()}
      exact
      path={eachRoute["path"]}
      component={eachRoute["component"]}
      {...eachRoute["options"]}
    />
  ));
  createdRoutes.push(
    <Route
      key={"route_999"}
      exact
      path={"**"}
      component={ROUTES[0]["component"]}
      {...ROUTES[0]["options"]}
    />
  );
  return createdRoutes;
};

export default createRoutes;
