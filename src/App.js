import logo from "./logo.svg";
import "./App.css";
import createRoutes from "./AppRouter";
import { Switch, BrowserRouter as Router } from "react-router-dom";
import Navbar from "./main/Components/Navbar/Navbar";

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>{createRoutes()}</Switch>
      </Router>
    </>
  );
}

export default App;
